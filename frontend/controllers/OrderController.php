<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 10.11.15
 * Time: 9:25
 */

namespace frontend\controllers;

use common\models\OrderAttribute;
use common\models\User;
use Yii;
use common\models\Order;
use yii\web\Controller;
use yii\base\View;
use yii\helpers\Url;
use common\models\UserAttributes;

class OrderController extends Controller
{

    public function actionCreate()
    {
        $msg = null;
        $modelOrder = new Order();


        /* If data sent via Pjax */
        if (Yii::$app->request->isPjax) {
            $data = Yii::$app->request->get();

            if (isset($data['loadUserData'])) {
                $userAttr = UserAttributes::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
                foreach ($userAttr as $attr) {
                    $modelOrder->{$attr->key} = $attr->value;
                }
                $modelOrder->isUserDataLoaded = true;
            } else {
                if ($modelOrder->load(Yii::$app->request->post()) && $modelOrder->save()) {
                    $msg = Yii::t('app', 'The Order successfully created!');
                } else {
                    $msg = Yii::t('app', 'Ops, something wrong!');
                }
            }
        }

        return $this->render('create', [
                'modelOrder' => $modelOrder,
                'msg' => $msg,
        ]);
    }

}