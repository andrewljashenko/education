<?php

namespace frontend\components\widgets;

class OrderFormWidget extends \yii\base\Widget {

    public $model;
    public $msg;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('form', [
            'modelOrder' => $this->model,
            'msg' => $this->msg
        ]);
    }


} 