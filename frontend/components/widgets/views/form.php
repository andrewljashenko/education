<?php

namespace frontend\views\order;

use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

//Load Client Script for "Use Profile Data" checkbox
$this->registerJsFile(Url::base() . '/js/ajax-order-form.js', [
        'position' => \yii\web\View::POS_END,
        'depends' => [\yii\web\JqueryAsset::className()]
]);

?>

<?php Pjax::begin(); ?>

<?php $form = ActiveForm::begin([
        'action' => ['order/create'],
        'options' => ['data-pjax' => true ]
]); ?>

<?= $form->field($modelOrder, 'first_name')->textInput(); ?>
<?= $form->field($modelOrder, 'last_name')->textInput(); ?>
<?= $form->field($modelOrder, 'address')->textInput(); ?>
<?=Html::activeHiddenInput($modelOrder, 'user_id', ['value' => Yii::$app->user->id]); ?>

<?php if ( ! Yii::$app->user->isGuest ) : ?>
    <?= $form->field($modelOrder, 'isUserDataLoaded')->checkbox([
            'label' => Yii::t('app', 'Use Profile Data'),
            'class' => ['use-profile-data'],
    ]);
    ?>
<?php endif; ?>

    <br/>
<?= Html::submitButton(); ?>

<?php ActiveForm::end(); ?>

<?php echo $msg; ?>

<?php Pjax::end(); ?>