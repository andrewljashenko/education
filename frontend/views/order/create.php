<?php

namespace frontend\views\order;

use frontend\components\widgets\OrderFormWidget;

echo OrderFormWidget::widget(['model' => $modelOrder, 'msg' => $msg]);