<?php

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use dosamigos\google\maps\services\DirectionsClient;
use dosamigos\google\maps\services\GeocodingClient;

if ($data) {
    foreach ($data as $key => $value) {

        /* Search for coordinates of address by address string */
        $geoCoding = new GeocodingClient([
            'params' => ['address' => $value['address']],
        ]);

        $result = $geoCoding->lookup();

        /* If search is success */
        if ($result['status'] == 'OK') {
            $location = $result['results'][0]['geometry']['location'];
            $coordinate = new LatLng(['lat' => $location['lat'], 'lng' => $location['lng']]);

            //Create new Map
            $map = new Map([
                'center' => $coordinate,
                'zoom' => 14,
            ]);

            $marker = new Marker([
                'position' => $coordinate,
            ]);

            $map->addOverlay($marker);

            echo $map->display();
        }
    }
}