<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => Yii::t('app', 'Manage Products'), 'icon' => 'fa fa-shopping-cart', 'url' => ['product/index'],
                        'items' => [
                                ['label' => Yii::t('app', 'New Product'), 'icon' => 'fa fa-plus', 'url' => ['product/create']],
                                ['label' => Yii::t('app', 'Products List'), 'icon' => 'fa fa-list', 'url' => ['product/index']],
                        ]
                    ],
                    ['label' => Yii::t('app', 'Manage Categories'), 'icon' => 'fa fa-building', 'url' => ['category/index'],
                        'items' => [
                                ['label' => Yii::t('app', 'New Category'), 'icon' => 'fa fa-plus', 'url' => ['category/create']],
                                ['label' => Yii::t('app', 'Categories List'), 'icon' => 'fa fa-list', 'url' => ['category/index']],
                        ]
                    ],
                    ['label' => Yii::t('app', 'Manage Terms'), 'icon' => 'fa fa-tags', 'url' => ['term/index'],
                            'items' => [
                                    ['label' => Yii::t('app', 'New Term'), 'icon' => 'fa fa-plus', 'url' => ['term/create']],
                                    ['label' => Yii::t('app', 'Terms List'), 'icon' => 'fa fa-list', 'url' => ['term/index']],
                            ]
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
