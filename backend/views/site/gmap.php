<?php

$form = \yii\widgets\ActiveForm::begin(); ?>

    <?= $form->field($model, 'address')->textInput(); ?>
    <?= \yii\helpers\Html::submitButton(); ?>

<?php \yii\widgets\ActiveForm::end();

echo $msg;

echo \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider
]);
