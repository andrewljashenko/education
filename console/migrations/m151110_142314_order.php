<?php

use yii\db\Schema;
use yii\db\Migration;

class m151110_142314_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order}}', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(),
                'first_name' => $this->string()->notNull(),
                'last_name' => $this->string()->notNull(),
                'address' => $this->string(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m151110_142314_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
