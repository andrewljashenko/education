<?php

namespace common\models;

use Yii;

class Gmap extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%gmap}}';
    }

    public function rules()
    {
        return [
            ['address', 'required']
        ];
    }
}